#!/bin/bash

exec >> /dev/tty1
exec 2>&1

COMPANY=$(curl -s http://whatismyip.akamai.com)
ID=vm_qcow2
DEVICE=/dev/vda

apk add --no-cache python3 fio py3-six gnuplot curl lshw libc6-compat

cd /root/app
cp distro/alpine/fio2gnuplot /usr/bin/

for JOB in "tests"/*; do
    echo "----> Executing job: $JOB"
    rm -rf output
    mkdir output
    rm -rf /mnt/fio-test
    mkdir /mnt/fio-test

    lshw -json -sanitize > output/hardware.json

    iostat -czk -d $DEVICE 1 > output/iostat.output &
    fio --output=output/fio.output $JOB
    pkill iostat

    PLOTNAME=$(basename -- "$JOB")
    PLOTNAME="${PLOTNAME%.*}"

    cd output
    fio2gnuplot -t $PLOTNAME-bw -b -g -p '*_bw*'
    fio2gnuplot -t $PLOTNAME-iops -i -g -p '*_iops*'
    cd ..

    ./upload upload --address isardvdi.com:1313 \
                    --root-certificate certs/localhost.cert \
                    --path output \
                    --company $COMPANY \
                    --id $ID \
                    --test $PLOTNAME
done
rm -rf /mnt/fio-test
cd /root
rm -rf app
poweroff