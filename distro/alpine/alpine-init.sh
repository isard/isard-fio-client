#!/bin/sh

exec >> /dev/tty1
exec 2>&1

# echo "Activate community repo for "
# sudo sed -e 's;^#http\(.*\)/v3\(.*\)/community;http\1/v3\2/community;g' \
#        -i /etc/apk/repositories
# apk update

REPO="https://gitlab.com/isard/isard-fio-client"
apk add git bash
cd /root
rm -rf app
git clone $REPO app
cd app
cp distro/alpine/start.sh .
/bin/bash start.sh
