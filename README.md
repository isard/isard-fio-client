# Install

Files in this repo by now are intended for Alpine 3.14 image only

## Download and create desktop in IsardVDI from virt optimized:

- https://dl-cdn.alpinelinux.org/alpine/v3.14/releases/x86_64/alpine-virt-3.14.0-x86_64.iso

## Community repo

Activate ../community repo (only second line) at repositories:

```
vi /etc/apk/repositories
apk update
```
or (not tested)

```
echo "Activate community repo for "
sudo sed -e 's;^#http\(.*\)/v3\(.*\)/community;http\1/v3\2/community;g' \
       -i /etc/apk/repositories
apk update
```

## Add custom service script

wget https://gitlab.com/isard/isard-fio-client/-/raw/main/distro/alpine/alpine-init.sh -O /root/init.sh
chmod 744 /root/init.sh
wget https://gitlab.com/isard/isard-fio-client/-/raw/main/distro/alpine/custom-service -O /etc/init.d/custom-service
chmod 744 /etc/init.d/custom-service
rc-update add custom-service default

## Reboot and test